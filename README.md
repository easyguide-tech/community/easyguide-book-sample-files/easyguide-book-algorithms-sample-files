EasyGuide book algorithms sample files - EasyGuide.tech
===
Source code examples
---

The package is Open core Source released under the [MIT Expat](LICENSE) with DCO requirement for contributing (Developer Certificate of Origin).

It is developed by cheikhna diouf and derived from his official book "[NAME]" which you can buy as printed book or as ebook on official website, Amazon, Google Play and the iBooks Store.

* Website : https://easyguide.tech

# Copyright and License
---
* This project is released under the MIT expat license - see the [LICENSE.md](LICENSE.md) file for details.